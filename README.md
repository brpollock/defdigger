# Defold Digger

This is a port of [digger](https://www.openprocessing.org/sketch/146073) to Defold.

Controls:

- left mouse button controls the player

It uses:

- a tilemap
- a tilesource
- a png (for the tiles)
- one 'main loop' script